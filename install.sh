#!/bin/bash

#############################################
# Copyright 2023 user0                      #
# SPDX-License-Identifier: GPL-3.0-or-later #
################################################################################################

# Directories & Files

# Squeekboard-Layouts directory
REPO=$(find ~/ -ipath '*/Squeekboard-Layouts')

# YAML Directories
REPO_DEFAULT=$REPO/Default-Color
REPO_CUSTOM=$REPO/Custom-Color

# GTK Directories
REPO_THEMES=$REPO/gtk-3.0/themes
REPO_COLORS=$REPO_THEMES/colors
REPO_OSK=$REPO_THEMES/osk

# GTK Style Files
REPO_STYLE_00=$REPO_THEMES/glassy-style.css

# GTK Theme Files
REPO_THEME_00=$REPO_COLORS/pink-glass.css
REPO_THEME_01=$REPO_COLORS/red-glass.css
REPO_THEME_02=$REPO_COLORS/orange-glass.css
REPO_THEME_03=$REPO_COLORS/yellow-glass.css
REPO_THEME_04=$REPO_COLORS/green-glass.css
REPO_THEME_05=$REPO_COLORS/blue-glass.css
REPO_THEME_06=$REPO_COLORS/purple-glass.css
REPO_THEME_07=$REPO_COLORS/berry-glass.css
REPO_THEME_08=$REPO_COLORS/mint_light-glass.css
REPO_THEME_09=$REPO_COLORS/brown-glass.css

# GTK OSK Files
SQUEEKBOARD_COLORS=$REPO_OSK/squeekboard_colors.css
SQUEEKBOARD_RS_100=$REPO_OSK/scale_1.0_and_2.0/squeekboard_resizing.css
SQUEEKBOARD_RS_125=$REPO_OSK/scale_1.25/squeekboard_resizing.css
SQUEEKBOARD_RS_150=$REPO_OSK/scale_1.5/squeekboard_resizing.css
SQUEEKBOARD_RS_175=$REPO_OSK/scale_1.75/squeekboard_resizing.css

# Default-Color Deutsch YAML Files
DEFAULT_DE_YAML_100=$REPO_DEFAULT/Scale_1.0_and_2.0/de+l5.yaml
DEFAULT_DE_YAML_125=$REPO_DEFAULT/Scale_1.25/de+l5.yaml
DEFAULT_DE_YAML_150=$REPO_DEFAULT/Scale_1.5/de+l5.yaml
DEFAULT_DE_YAML_175=$REPO_DEFAULT/Scale_1.75/de+l5.yaml
DEFAULT_DE_YAML_100_WIDE=$REPO_DEFAULT/Scale_1.0_and_2.0/de+l5_wide.yaml
DEFAULT_DE_YAML_125_WIDE=$REPO_DEFAULT/Scale_1.25/de+l5_wide.yaml
DEFAULT_DE_YAML_150_WIDE=$REPO_DEFAULT/Scale_1.5/de+l5_wide.yaml
DEFAULT_DE_YAML_175_WIDE=$REPO_DEFAULT/Scale_1.75/de+l5_wide.yaml

# Custom-Color Deutsch YAML Files
CUSTOM_DE_YAML_100=$REPO_CUSTOM/Scale_100%_and_200%/de+l5.yaml
CUSTOM_DE_YAML_125=$REPO_CUSTOM/Scale_125%/de+l5.yaml
CUSTOM_DE_YAML_150=$REPO_CUSTOM/Scale_150%/de+l5.yaml
CUSTOM_DE_YAML_175=$REPO_CUSTOM/Scale_175%/de+l5.yaml
CUSTOM_DE_YAML_100_WIDE=$REPO_CUSTOM/Scale_100%_and_200%/de+l5_wide.yaml
CUSTOM_DE_YAML_125_WIDE=$REPO_CUSTOM/Scale_125%/de+l5_wide.yaml
CUSTOM_DE_YAML_150_WIDE=$REPO_CUSTOM/Scale_150%/de+l5_wide.yaml
CUSTOM_DE_YAML_175_WIDE=$REPO_CUSTOM/Scale_175%/de+l5_wide.yaml

# Default-Color English(US) YAML Files
DEFAULT_US_YAML_100=$REPO_DEFAULT/Scale_1.0_and_2.0/us+l5.yaml
DEFAULT_US_YAML_125=$REPO_DEFAULT/Scale_1.25/us+l5.yaml
DEFAULT_US_YAML_150=$REPO_DEFAULT/Scale_1.5/us+l5.yaml
DEFAULT_US_YAML_175=$REPO_DEFAULT/Scale_1.75/us+l5.yaml
DEFAULT_US_YAML_100_WIDE=$REPO_DEFAULT/Scale_1.0_and_2.0/us+l5_wide.yaml
DEFAULT_US_YAML_125_WIDE=$REPO_DEFAULT/Scale_1.25/us+l5_wide.yaml
DEFAULT_US_YAML_150_WIDE=$REPO_DEFAULT/Scale_1.5/us+l5_wide.yaml
DEFAULT_US_YAML_175_WIDE=$REPO_DEFAULT/Scale_1.75/us+l5_wide.yaml

# Custom-Color English(US) YAML Files
CUSTOM_US_YAML_100=$REPO_CUSTOM/Scale_100%_and_200%/us+l5.yaml
CUSTOM_US_YAML_125=$REPO_CUSTOM/Scale_125%/us+l5.yaml
CUSTOM_US_YAML_150=$REPO_CUSTOM/Scale_150%/us+l5.yaml
CUSTOM_US_YAML_175=$REPO_CUSTOM/Scale_175%/us+l5.yaml
CUSTOM_US_YAML_100_WIDE=$REPO_CUSTOM/Scale_100%_and_200%/us+l5_wide.yaml
CUSTOM_US_YAML_125_WIDE=$REPO_CUSTOM/Scale_125%/us+l5_wide.yaml
CUSTOM_US_YAML_150_WIDE=$REPO_CUSTOM/Scale_150%/us+l5_wide.yaml
CUSTOM_US_YAML_175_WIDE=$REPO_CUSTOM/Scale_175%/us+l5_wide.yaml

#-----------------------------------------------------------------------------------------------

# XML File
EVDEV_XML=/usr/share/X11/xkb/rules/evdev.xml

# Squeekboard directories
KEYBOARDS=~/.local/share/squeekboard/keyboards
TERMINAL=$KEYBOARDS/terminal

# GTK directories
GTK_3=~/.config/gtk-3.0
THEMES=$GTK_3/themes
COLORS=$THEMES/colors
OSK=$THEMES/osk
SCALE_100=$OSK/scale_1.0_and_2.0
SCALE_125=$OSK/scale_1.25
SCALE_150=$OSK/scale_1.5
SCALE_175=$OSK/scale_1.75

# GTK files
GTK_CSS=$GTK_3/gtk.css
STYLE_00=$THEMES/glassy-style.css
THEME_00=$COLORS/pink-glass.css
THEME_01=$COLORS/red-glass.css
THEME_02=$COLORS/orange-glass.css
THEME_03=$COLORS/yellow-glass.css
THEME_04=$COLORS/green-glass.css
THEME_05=$COLORS/blue-glass.css
THEME_06=$COLORS/purple-glass.css
THEME_07=$COLORS/berry-glass.css
THEME_08=$COLORS/mint_light-glass.css
THEME_09=$COLORS/brown-glass.css
SB_COLORS=$OSK/squeekboard_colors.css
SB_RS_100=$SCALE_100/squeekboard_resizing.css
SB_RS_125=$SCALE_125/squeekboard_resizing.css
SB_RS_150=$SCALE_150/squeekboard_resizing.css
SB_RS_175=$SCALE_175/squeekboard_resizing.css

# Backups
EVDEV_XML_BACKUP=/usr/share/X11/xkb/rules/evdev.xml.bak
KEYBOARDS_BACKUP=~/.keyboards.backup/
GTK_CSS_BACKUP=$GTK_3/gtk.bak

################################################################################################

# Arrays & Variables

# languages array
languages[0]="German"
languages[1]="English (US)"

# langs array
langs[0]="de"
langs[1]="us"

# keyboards array
keyboards[0]="Standard"
keyboards[1]="Terminal"
keyboards[2]="Both"

# scales array
scales[0]="100%"
scales[1]="125%"
scales[2]="150%"
scales[3]="175%"
scales[4]="200%"

# scale_directories array
scale_directories[0]="scale_1.0_and_2.0"
scale_directories[1]="scale_1.25"
scale_directories[2]="scale_1.5"
scale_directories[3]="scale_1.75"

# scale_locations array
scale_locations[0]="$SCALE_100"
scale_locations[1]="$SCALE_125"
scale_locations[2]="$SCALE_150"
scale_locations[3]="$SCALE_175"

# adjustments array
adjustments[0]="No"
adjustments[1]="Apply"

# resizing_imports array
resizing_imports[0]="@import url(\"themes/osk/scale_1.0_and_2.0/squeekboard_resizing.css\");"
resizing_imports[1]="@import url(\"themes/osk/scale_1.25/squeekboard_resizing.css\");"
resizing_imports[2]="@import url(\"themes/osk/scale_1.5/squeekboard_resizing.css\");"
resizing_imports[3]="@import url(\"themes/osk/scale_1.75/squeekboard_resizing.css\");"

# style_resizing_imports array
style_resizing_imports[0]="@import url(\"osk/scale_1.0_and_2.0/squeekboard_resizing.css\");"
style_resizing_imports[1]="@import url(\"osk/scale_1.25/squeekboard_resizing.css\");"
style_resizing_imports[2]="@import url(\"osk/scale_1.5/squeekboard_resizing.css\");"
style_resizing_imports[3]="@import url(\"osk/scale_1.75/squeekboard_resizing.css\");"

# resizing_locations array
resizing_locations[0]="$SB_RS_100"
resizing_locations[1]="$SB_RS_125"
resizing_locations[2]="$SB_RS_150"
resizing_locations[3]="$SB_RS_175"

# colors array
colors[0]="Default"
colors[1]="Custom"

# themes array
themes[0]="Pink Glass"
themes[1]="Red Glass"
themes[2]="Orange Glass"
themes[3]="Yellow Glass"
themes[4]="Green Glass"
themes[5]="Blue Glass"
themes[6]="Purple Glass"
themes[7]="Berry Glass"
themes[8]="Mint Light Glass"
themes[9]="Brown Glass"

# theme_imports array
theme_imports[0]="@import url(\"colors/pink-glass.css\");"
theme_imports[1]="@import url(\"colors/red-glass.css\");"
theme_imports[2]="@import url(\"colors/orange-glass.css\");"
theme_imports[3]="@import url(\"colors/yellow-glass.css\");"
theme_imports[4]="@import url(\"colors/green-glass.css\");"
theme_imports[5]="@import url(\"colors/blue-glass.css\");"
theme_imports[6]="@import url(\"colors/purple-glass.css\");"
theme_imports[7]="@import url(\"colors/berry-glass.css\");"
theme_imports[8]="@import url(\"colors/mint_light-glass.css\");"
theme_imports[9]="@import url(\"colors/brown-glass.css\");"

# repo_theme_locations array
repo_theme_locations[0]="$REPO_THEME_00"
repo_theme_locations[1]="$REPO_THEME_01"
repo_theme_locations[2]="$REPO_THEME_02"
repo_theme_locations[3]="$REPO_THEME_03"
repo_theme_locations[4]="$REPO_THEME_04"
repo_theme_locations[5]="$REPO_THEME_05"
repo_theme_locations[6]="$REPO_THEME_06"
repo_theme_locations[7]="$REPO_THEME_07"
repo_theme_locations[8]="$REPO_THEME_08"
repo_theme_locations[9]="$REPO_THEME_09"

# theme_locations array
theme_locations[0]="$THEME_00"
theme_locations[1]="$THEME_01"
theme_locations[2]="$THEME_02"
theme_locations[3]="$THEME_03"
theme_locations[4]="$THEME_04"
theme_locations[5]="$THEME_05"
theme_locations[6]="$THEME_06"
theme_locations[7]="$THEME_07"
theme_locations[8]="$THEME_08"
theme_locations[9]="$THEME_09"

# styles array
styles[0]="Glassy"

# style_imports array
style_imports[0]="@import url(\"themes/glassy-style.css\");"

# repo_style_locations array
repo_style_locations[0]="$REPO_STYLE_00"

# style_locations array
style_locations[0]="$STYLE_00"

# css array
css[0]="glassy-style.css"

# backups array
backups[0]="Backups have been created here:"
backups[1]="You already have backups here:"

# phosh array
phosh[0]="No restart required"
phosh[1]="Restart phosh to apply latest CSS files"
phosh[2]="Phosh must be restarted to apply layout"

# restart array
restart[0]="Restart phosh now"

# variables
declare -- LANGUAGE
declare -- LANG
declare -- KEYBOARD
declare -- SCALE
declare -- ADJUSTMENTS
declare -- RESIZING_IMPORT
declare -- STYLE_RESIZING_IMPORT
declare -- COLOR
declare -- THEME
declare -- THEME_IMPORT
declare -- THEME_CSS
declare -- REPO_THEME_CSS
declare -- STYLE
declare -- STYLE_IMPORT
declare -- STYLE_CSS
declare -- REPO_STYLE_CSS
declare -- CSS
declare -- BACKUPS
declare -- PHOSH
declare -- RESTART

SB_COLORS_IMPORT="@import url(\"osk/squeekboard_colors.css\");"

SBRS="squeekboard_resizing.css"

SCALE_0=${scale_directories[0]}
SCALE_1=${scale_directories[1]}
SCALE_2=${scale_directories[2]}
SCALE_3=${scale_directories[3]}

################################################################################################

# Ask user to select language

clear
echo ""
echo " Which language to use?"
echo ""
echo " 1) * Deutsch *"
echo " 2)   English(US)"
echo " q)   I change my mind; quit."
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
read a;
case $a in
    q|Q)
        clear
        exit
        ;;
    1|'')
        LANGUAGE=${languages[0]}
        LANG=${langs[0]}
        clear
        echo ""
        echo ""
        echo ""
        echo "      $LANGUAGE keyboard"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    2)
        LANGUAGE=${languages[1]}
        LANG=${langs[1]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $LANGUAGE keyboard"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
esac

################################################################################################

# Ask user to select keyboard

clear
echo ""
echo " Which keyboard to customize?"
echo ""
echo " 1) * Standard keyboard *"
echo " 2)   Terminal keyboard"
echo " 3)   Both keyboards"
echo " q)   I change my mind; quit."
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
read a;
case $a in
    q|Q)
        clear
        exit
        ;;
    1|'')
        KEYBOARD=${keyboards[0]}
        clear
        echo ""
        echo ""
        echo ""
        echo "      $KEYBOARD keyboard"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    2)
        KEYBOARD=${keyboards[1]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $KEYBOARD keyboard"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    3)
        KEYBOARD=${keyboards[2]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $KEYBOARD keyboards"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
esac

################################################################################################

# Ask user to select scale

clear
echo ""
echo " Which display scale are you using?"
echo ""
echo " 1)   100%"
echo " 2)   125%"
echo " 3)   150%"
echo " 4)   175%"
echo " 5)   200% (Default)"
echo " q)   I change my mind; quit."
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
read a;
case $a in
    q|Q)
        clear
        exit
        ;;
    1)
        SCALE=${scales[0]}
        clear
        echo ""
        echo ""
        echo ""
        echo "      $SCALE scale"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    2)
        SCALE=${scales[1]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $SCALE scale"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    3)
        SCALE=${scales[2]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $SCALE scale"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    4)
        SCALE=${scales[3]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $SCALE scale"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    5|'')
        SCALE=${scales[4]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $SCALE scale"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
esac

################################################################################################

# Ask user to select colors

clear
echo ""
echo " Which colors would you like?"
echo ""
echo " 0)   I don't know; surprise me."
echo " 1)   Default colors"
echo " 2) * Custom colors *"
echo " q)   I change my mind; quit."
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
read a;
case $a in
    q|Q)
        clear
        exit
        ;;
    0)
        RANDOM_NUMBER=$[$RANDOM % ${#colors[@]}]
        RANDOM_COLOR=${colors[RANDOM_NUMBER]}
        COLOR=$RANDOM_COLOR
        clear
        echo ""
        echo ""
        echo ""
        echo "      $COLOR colors"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    1)
        COLOR=${colors[0]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $COLOR colors"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    2|'')
        COLOR=${colors[1]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $COLOR colors"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
esac

################################################################################################

# Ask user to select color theme preference

if [[ $COLOR == ${colors[1]} ]]; then
    clear
    echo ""
    echo " Which color theme would you like?"
    echo ""
    echo " 0)   I don't know; surprise me."
    echo " 1) * Pink Glass *"
    echo " 2)   Red Glass"
    echo " 3)   Orange Glass"
    echo " 4)   Yellow Glass"
    echo " 5)   Green Glass"
    echo " 6)   Blue Glass"
    echo " 7)   Purple Glass"
    echo " 8)   Berry Glass"
    echo " 9)   Mint Light Glass"
    echo "10)   Brown Glass"
    echo " q)   I change my mind; quit."
    echo ""
    read a;
    case $a in
        q|Q)
            clear
            exit
            ;;
    0)
        RANDOM_NUMBER=$[$RANDOM % ${#themes[@]}]
        RANDOM_THEME=${themes[RANDOM_NUMBER]}
        THEME=$RANDOM_THEME
        clear
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    1|'')
        THEME=${themes[0]}
        THEME_IMPORT=${theme_imports[0]}
        THEME_CSS=${theme_locations[0]}
        REPO_THEME_CSS=${repo_theme_locations[0]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    2)
        THEME=${themes[1]}
        THEME_IMPORT=${theme_imports[1]}
        THEME_CSS=${theme_locations[1]}
        REPO_THEME_CSS=${repo_theme_locations[1]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    3)
        THEME=${themes[2]}
        THEME_IMPORT=${theme_imports[2]}
        THEME_CSS=${theme_locations[2]}
        REPO_THEME_CSS=${repo_theme_locations[2]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    4)
        THEME=${themes[3]}
        THEME_IMPORT=${theme_imports[3]}
        THEME_CSS=${theme_locations[3]}
        REPO_THEME_CSS=${repo_theme_locations[3]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    5)
        THEME=${themes[4]}
        THEME_IMPORT=${theme_imports[4]}
        THEME_CSS=${theme_locations[4]}
        REPO_THEME_CSS=${repo_theme_locations[4]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    6)
        THEME=${themes[5]}
        THEME_IMPORT=${theme_imports[5]}
        THEME_CSS=${theme_locations[5]}
        REPO_THEME_CSS=${repo_theme_locations[5]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    7)
        THEME=${themes[6]}
        THEME_IMPORT=${theme_imports[6]}
        THEME_CSS=${theme_locations[6]}
        REPO_THEME_CSS=${repo_theme_locations[6]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    8)
        THEME=${themes[7]}
        THEME_IMPORT=${theme_imports[7]}
        THEME_CSS=${theme_locations[7]}
        REPO_THEME_CSS=${repo_theme_locations[7]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    9)
        THEME=${themes[8]}
        THEME_IMPORT=${theme_imports[8]}
        THEME_CSS=${theme_locations[8]}
        REPO_THEME_CSS=${repo_theme_locations[8]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        echo ""
        sleep 1
        ;;
    10)
        THEME=${themes[9]}
        THEME_IMPORT=${theme_imports[9]}
        THEME_CSS=${theme_locations[9]}
        REPO_THEME_CSS=${repo_theme_locations[9]}
        clear
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo "      $THEME theme"
        echo ""
        echo ""
        sleep 1
        ;;
    esac
fi

################################################################################################

# If more styles are added, then remove this section and ask user to select style
if [[ $COLOR == ${colors[1]} ]]; then
    STYLE=${styles[0]}
    STYLE_IMPORT=${style_imports[0]}
    STYLE_CSS=${style_locations[0]}
    REPO_STYLE_CSS=${repo_style_locations[0]}
fi

################################################################################################

# Ask user to select adjustment preference

if [[ $COLOR == ${colors[0]} ]]; then
    clear
    echo ""
    echo " Apply adjustments based on scale?"
    echo ""
    echo " 0)   I don't know; surprise me."
    echo " 1) * Yes *"
    echo " 2)   No"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        q|Q)
            clear
            exit
            ;;
        0)
            RANDOM_NUMBER=$[$RANDOM % ${#colors[@]}]
            RANDOM_ADJUSTMENTS=${adjustments[RANDOM_NUMBER]}
            ADJUSTMENTS=$RANDOM_ADJUSTMENTS
            clear
            echo ""
            echo ""
            echo ""
            echo "      $ADJUSTMENTS adjustments"
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            sleep 1
            ;;
        1|'')
            ADJUSTMENTS=${adjustments[1]}
            clear
            echo ""
            echo ""
            echo ""
            echo ""
            echo "      $ADJUSTMENTS adjustments"
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            sleep 1
            ;;
        2)
            ADJUSTMENTS=${adjustments[0]}
            clear
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo "      $ADJUSTMENTS adjustments"
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            sleep 1
            ;;
    esac

#-----------------------------------------------------------------------------------------------

# Explicitly set ADJUSTMENTS variable to Apply if user selected Custom colors
elif [[ $COLOR == ${colors[1]} ]]; then
    ADJUSTMENTS=${adjustments[1]}
fi

################################################################################################

# Set resizing variables

if [[ $ADJUSTMENTS == ${adjustments[1]} ]]; then

    # Default-Color
    if [[ $COLOR == ${colors[0]} ]]; then

        # Scales
        if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
            RESIZING_IMPORT=${resizing_imports[0]}
        elif [[ $SCALE == ${scales[1]} ]]; then
            RESIZING_IMPORT=${resizing_imports[1]}
        elif [[ $SCALE == ${scales[2]} ]]; then
            RESIZING_IMPORT=${resizing_imports[2]}
        elif [[ $SCALE == ${scales[3]} ]]; then
            RESIZING_IMPORT=${resizing_imports[3]}
        fi

    # Custom-Color
    elif [[ $COLOR == ${colors[1]} ]]; then

        # Scales
        if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
            STYLE_RESIZING_IMPORT=${style_resizing_imports[0]}
        elif [[ $SCALE == ${scales[1]} ]]; then
            STYLE_RESIZING_IMPORT=${style_resizing_imports[1]}
        elif [[ $SCALE == ${scales[2]} ]]; then
            STYLE_RESIZING_IMPORT=${style_resizing_imports[2]}
        elif [[ $SCALE == ${scales[3]} ]]; then
            STYLE_RESIZING_IMPORT=${style_resizing_imports[3]}
        fi
    fi
fi

################################################################################################

# Set phosh restart variable

# If no adjustments are applied, then phosh restart is not required or recommended
if [[ $ADJUSTMENTS == ${adjustments[0]} ]]; then
    PHOSH=${phosh[0]}
else
    PHOSH=${phosh[1]}
fi

################################################################################################

# Inform user of password prompt
if [[ ! -f $EVDEV_XML_BACKUP ]]; then
    clear
    echo ""
    echo " You will be prompted for a password"
    echo " in order to copy the following file:"
    echo ""
    echo "  $EVDEV_XML"
    echo ""
    echo "              Continue?"
    echo ""
    echo ""
    echo " y) * Yes *"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            ;;
        q|Q)
            clear
            exit
            ;;
    esac
    BACKUPS=${backups[0]}
else
    BACKUPS=${backups[1]}
fi

################################################################################################

# Backups

if [[ ! -f $EVDEV_XML_BACKUP ]]; then

    # Squeekboard
    if [[ ! -d $KEYBOARDS_BACKUP && -d $KEYBOARDS ]]; then
        cp -rf $KEYBOARDS $KEYBOARDS_BACKUP
    fi

    # GTK
    if [[ ! -f $GTK_CSS_BACKUP && -f $GTK_CSS ]]; then
        cp $GTK_CSS $GTK_CSS_BACKUP
    fi

    # XML
    sudo cp $EVDEV_XML $EVDEV_XML_BACKUP
fi

################################################################################################

# Inform user of backup and install

if [[ -f $EVDEV_XML_BACKUP && -d $KEYBOARDS_BACKUP && -f $GTK_CSS_BACKUP ]]; then
    clear
    echo ""
    echo "   $BACKUPS"
    echo ""
    echo "   $KEYBOARDS_BACKUP"
    echo " $GTK_CSS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo "    Install Squeekboard-Layouts?"
    echo ""
    echo " y) * Yes *"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            ;;
        q|Q)
            clear
            exit
            ;;
    esac

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP && -d $KEYBOARDS_BACKUP ]]; then
    clear
    echo ""
    echo "   $BACKUPS"
    echo ""
    echo "   $KEYBOARDS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo "    Install Squeekboard-Layouts?"
    echo ""
    echo ""
    echo " y) * Yes *"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            ;;
        q|Q)
            clear
            exit
            ;;
    esac

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP && -f $GTK_CSS_BACKUP ]]; then
    clear
    echo ""
    echo "   $BACKUPS"
    echo ""
    echo " $GTK_CSS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo "    Install Squeekboard-Layouts?"
    echo ""
    echo ""
    echo " y) * Yes *"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            ;;
        q|Q)
            clear
            exit
            ;;
    esac

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP ]]; then
    clear
    echo ""
    echo "    $BACKUPS"
    echo ""
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo "    Install Squeekboard-Layouts?"
    echo ""
    echo ""
    echo ""
    echo " y) * Yes *"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            ;;
        q|Q)
            clear
            exit
            ;;
    esac
fi

################################################################################################

# Inform user of password prompt

if ! grep -q "$LANGUAGE (Librem 5)" "$EVDEV_XML"; then
    if [[ $BACKUPS == ${backups[1]} ]]; then
        clear
        echo ""
        echo " You will be prompted for a password"
        echo " in order to edit the following file:"
        echo ""
        echo "  $EVDEV_XML"
        echo ""
        echo "              Continue?"
        echo ""
        echo ""
        echo " y) * Yes *"
        echo " q)   I change my mind; quit."
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        read a;
        case $a in
            y|Y|'')
                ;;
            q|Q)
                clear
                exit
                ;;
        esac
    fi
fi

################################################################################################

# XML File

# Add selected language to evdev.xml if it has not yet been added
if ! grep -q "$LANGUAGE (Librem 5)" "$EVDEV_XML"; then
    sudo sed -i "\|<description>$LANGUAGE</description>|,\|<variantList>| s|<variantList>|<variantList>\\n        <variant>\\n          <configItem>\\n            <name>l5</name>\\n            <description>$LANGUAGE (Librem 5)</description>\\n          </configItem>\\n        </variant>|" $EVDEV_XML
fi

################################################################################################

# Squeekboard Directories

# Create Squeekboard directories if they do not yet exist
if [[ $KEYBOARD == ${keyboards[1]} || $KEYBOARD == ${keyboards[2]} ]]; then
    if [[ ! -d $TERMINAL ]]; then
        mkdir -p $TERMINAL
    fi
elif [[ ! -d $KEYBOARDS ]]; then
    mkdir -p $KEYBOARDS
fi

################################################################################################

# Install YAML Files

# Default-Color
if [[ $COLOR == ${colors[0]} ]]; then

    # Standard Keyboard or Both Keyboards
    if [[ $KEYBOARD == ${keyboards[0]} || $KEYBOARD == ${keyboards[2]} ]]; then

        # Deutsch
        if [[ $LANGUAGE == ${languages[0]} ]]; then

            # Copy Deutsch YAML files of selected scale to keyboards directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $DEFAULT_DE_YAML_100 $KEYBOARDS
                cp $DEFAULT_DE_YAML_100_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $DEFAULT_DE_YAML_125 $KEYBOARDS
                cp $DEFAULT_DE_YAML_125_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $DEFAULT_DE_YAML_150 $KEYBOARDS
                cp $DEFAULT_DE_YAML_150_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $DEFAULT_DE_YAML_175 $KEYBOARDS
                cp $DEFAULT_DE_YAML_175_WIDE $KEYBOARDS
            fi
        fi

        # English(US)
        if [[ $LANGUAGE == ${languages[1]} ]]; then

            # Copy English(US) YAML files of selected scale to keyboards directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $DEFAULT_US_YAML_100 $KEYBOARDS
                cp $DEFAULT_US_YAML_100_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $DEFAULT_US_YAML_125 $KEYBOARDS
                cp $DEFAULT_US_YAML_125_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $DEFAULT_US_YAML_150 $KEYBOARDS
                cp $DEFAULT_US_YAML_150_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $DEFAULT_US_YAML_175 $KEYBOARDS
                cp $DEFAULT_US_YAML_175_WIDE $KEYBOARDS
            fi
        fi
    fi

    # Terminal Keyboard or Both Keyboards
    if [[ $KEYBOARD == ${keyboards[1]} || $KEYBOARD == ${keyboards[2]} ]]; then

        # Deutsch
        if [[ $LANGUAGE == ${languages[0]} ]]; then

            # Copy Deutsch YAML files of selected scale to terminal directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $DEFAULT_DE_YAML_100 $TERMINAL
                cp $DEFAULT_DE_YAML_100_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $DEFAULT_DE_YAML_125 $TERMINAL
                cp $DEFAULT_DE_YAML_125_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $DEFAULT_DE_YAML_150 $TERMINAL
                cp $DEFAULT_DE_YAML_150_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $DEFAULT_DE_YAML_175 $TERMINAL
                cp $DEFAULT_DE_YAML_175_WIDE $TERMINAL
            fi
        fi

        # English(US)
        if [[ $LANGUAGE == ${languages[1]} ]]; then

            # Copy English(US) YAML files of selected scale to keyboards directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $DEFAULT_US_YAML_100 $TERMINAL
                cp $DEFAULT_US_YAML_100_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $DEFAULT_US_YAML_125 $TERMINAL
                cp $DEFAULT_US_YAML_125_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $DEFAULT_US_YAML_150 $
                cp $DEFAULT_US_YAML_150_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $DEFAULT_US_YAML_175 $TERMINAL
                cp $DEFAULT_US_YAML_175_WIDE $TERMINAL
            fi
        fi
    fi

#-----------------------------------------------------------------------------------------------

# Custom-Color
elif [[ $COLOR == ${colors[1]} ]]; then

    # Standard Keyboard or Both Keyboards
    if [[ $KEYBOARD == ${keyboards[0]} || $KEYBOARD == ${keyboards[2]} ]]; then

        # Deutsch
        if [[ $LANGUAGE == ${languages[0]} ]]; then

            # Copy YAML files of selected scale to keyboards directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $CUSTOM_DE_YAML_100 $KEYBOARDS
                cp $CUSTOM_DE_YAML_100_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $CUSTOM_DE_YAML_125 $KEYBOARDS
                cp $CUSTOM_DE_YAML_125_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $CUSTOM_DE_YAML_150 $KEYBOARDS
                cp $CUSTOM_DE_YAML_150_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $CUSTOM_DE_YAML_175 $KEYBOARDS
                cp $CUSTOM_DE_YAML_175_WIDE $KEYBOARDS
            fi
        fi

        # English(US)
        if [[ $LANGUAGE == ${languages[1]} ]]; then

            # Copy YAML files of selected scale to keyboards directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $CUSTOM_US_YAML_100 $KEYBOARDS
                cp $CUSTOM_US_YAML_100_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $CUSTOM_US_YAML_125 $KEYBOARDS
                cp $CUSTOM_US_YAML_125_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $CUSTOM_US_YAML_150 $KEYBOARDS
                cp $CUSTOM_US_YAML_150_WIDE $KEYBOARDS
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $CUSTOM_US_YAML_175 $KEYBOARDS
                cp $CUSTOM_US_YAML_175_WIDE $KEYBOARDS
            fi
        fi
    fi

    # Terminal Keyboard or Both Keyboards
    if [[ $KEYBOARD == ${keyboards[1]} || $KEYBOARD == ${keyboards[2]} ]]; then

        # Deutsch
        if [[ $LANGUAGE == ${languages[0]} ]]; then

            # Copy YAML files of selected scale to terminal directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $CUSTOM_DE_YAML_100 $TERMINAL
                cp $CUSTOM_DE_YAML_100_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $CUSTOM_DE_YAML_125 $TERMINAL
                cp $CUSTOM_DE_YAML_125_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $CUSTOM_DE_YAML_150 $TERMINAL
                cp $CUSTOM_DE_YAML_150_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $CUSTOM_DE_YAML_175 $TERMINAL
                cp $CUSTOM_DE_YAML_175_WIDE $TERMINAL
            fi
        fi

        # English(US)
        if [[ $LANGUAGE == ${languages[1]} ]]; then

            # Copy YAML files of selected scale to terminal directory
            if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
                cp $CUSTOM_US_YAML_100 $TERMINAL
                cp $CUSTOM_US_YAML_100_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[1]} ]]; then
                cp $CUSTOM_US_YAML_125 $TERMINAL
                cp $CUSTOM_US_YAML_125_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[2]} ]]; then
                cp $CUSTOM_US_YAML_150 $TERMINAL
                cp $CUSTOM_US_YAML_150_WIDE $TERMINAL
            elif [[ $SCALE == ${scales[3]} ]]; then
                cp $CUSTOM_US_YAML_175 $TERMINAL
                cp $CUSTOM_US_YAML_175_WIDE $TERMINAL
            fi
        fi
    fi
fi

################################################################################################

# Install GTK Adjustments File

if [[ $ADJUSTMENTS == ${adjustments[1]} ]]; then

    # Install Scale Resizing (100%/200%)
    if [[ $SCALE == ${scales[0]} || $SCALE == ${scales[4]} ]]; then
        if [[ ! -d ${scale_locations[0]} ]]; then
            for DIRECTORY in "${scale_locations[@]}"; do
                if [[ -d $DIRECTORY ]]; then
                    rm -rf $DIRECTORY
                fi
            done
            mkdir -p ${scale_locations[0]}
        fi
        cp $SQUEEKBOARD_RS_100 ${scale_locations[0]}

    # Install Scale Resizing (125%)
    elif [[ $SCALE == ${scales[1]} ]]; then
        if [[ ! -d ${scale_locations[1]} ]]; then
            for DIRECTORY in "${scale_locations[@]}"; do
                if [[ -d $DIRECTORY ]]; then
                    rm -rf $DIRECTORY
                fi
            done
            mkdir -p ${scale_locations[1]}
        fi
        cp $SQUEEKBOARD_RS_125 ${scale_locations[1]}

    # Install Scale Resizing (150%)
    elif [[ $SCALE == ${scales[2]} ]]; then
        if [[ ! -d ${scale_locations[2]} ]]; then
            for DIRECTORY in "${scale_locations[@]}"; do
                if [[ -d $DIRECTORY ]]; then
                    rm -rf $DIRECTORY
                fi
            done
            mkdir -p ${scale_locations[2]}
        fi
        cp $SQUEEKBOARD_RS_150 ${scale_locations[2]}

    # Install Scale Resizing (175%)
    elif [[ $SCALE == ${scales[3]} ]]; then
        if [[ ! -d ${scale_locations[3]} ]]; then
            for DIRECTORY in "${scale_locations[@]}"; do
                if [[ -d $DIRECTORY ]]; then
                    rm -rf $DIRECTORY
                fi
            done
            mkdir -p ${scale_locations[3]}
        fi
        cp $SQUEEKBOARD_RS_175 ${scale_locations[3]}
    fi
fi

################################################################################################

# Enable/Disable GTK Files

# Default-Color
if [[ $COLOR == ${colors[0]} ]]; then

    # If gtk.css does not yet exist
    if [[ ! -f $GTK_CSS ]]; then

        # Enable Adjustments
        if [[ $ADJUSTMENTS == ${adjustments[1]} ]]; then
            echo "" > $GTK_CSS
            PHOSH=${phosh[2]}
            sed -i "s|^$|$RESIZING_IMPORT|" $GTK_CSS
        fi

    # If gtk.css exists
    elif [[ -f $GTK_CSS ]]; then

        # Remove unused styles, files, & directories
        if [[ -f $SB_COLORS ]]; then
            rm $SB_COLORS
        fi
        for ((i = 0; i < ${#styles[@]}; ++i)); do
            STYLE_CSS=$(echo "${style_locations[$i]}")
            STYLE_IMPORT=$(echo "${style_imports[$i]}")
            if [[ -f $STYLE_CSS ]]; then
                if grep -q "$SB_COLORS_IMPORT" "$STYLE_CSS"; then
                    sed -i "\|$SB_COLORS_IMPORT|d" $STYLE_CSS
                    PHOSH=${phosh[2]}
                fi
                for RS_IMPORT in "${style_resizing_imports[@]}"; do
                    if grep -q "$RS_IMPORT" "$STYLE_CSS"; then
                        sed -i "\|$RS_IMPORT|d" $STYLE_CSS
                        PHOSH=${phosh[2]}
                    fi
                done
                COUNT=$(grep -o "@import" "$STYLE_CSS" | wc -l)
                if [ $COUNT -le 1 ] ; then
                    rm $STYLE_CSS
                    if grep -q "$STYLE_IMPORT" "$GTK_CSS"; then
                        sed -i "\|$STYLE_IMPORT|d" $GTK_CSS
                        PHOSH=${phosh[2]}
                    fi
                fi
            fi
        done
        for ((i = 0; i < ${#themes[@]}; ++i)); do
            THEME_CSS=$(echo "${theme_locations[$i]}")
            THEME_IMPORT=$(echo "${theme_imports[$i]}")
            if [[ -f $THEME_CSS ]]; then
                TAG="REMOVE"
                for STYLE_CSS in "${style_locations[@]}"; do
                    if [[ -f $STYLE_CSS ]]; then
                        if grep -q "$THEME_IMPORT" "$STYLE_CSS"; then
                            TAG="IGNORE"
                        fi
                    fi
                done
                if [[ $TAG == "REMOVE" ]]; then
                    rm $THEME_CSS
                fi
            fi
        done
        if [[ -d $COLORS ]]; then
            if [[ ! $(ls -A $COLORS) ]]; then
                rm -rf $COLORS
            fi
        fi

        # Disable Adjustments
        if [[ $ADJUSTMENTS == ${adjustments[0]} ]]; then

            # Remove unused files & directories
            if [[ -d $OSK ]]; then
                rm -rf $OSK
            fi
            for RS_IMPORT in "${resizing_imports[@]}"; do
                if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                    sed -i "\|$RS_IMPORT|d" $GTK_CSS
                    PHOSH=${phosh[2]}
                fi
            done
            if ! grep -q "@import" "$GTK_CSS"; then
                rm $GTK_CSS
            fi
            if [[ -d $THEMES ]]; then
                if [[ ! $(ls -A $THEMES) ]]; then
                    rm -rf $THEMES
                fi
            fi
        fi

        # Enable Adjustments
        if [[ $ADJUSTMENTS == ${adjustments[1]} ]]; then
            if ! grep -q "$RESIZING_IMPORT" "$GTK_CSS"; then
                for RS_IMPORT in "${resizing_imports[@]}"; do
                    if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                        sed -i "\|$RS_IMPORT|d" $GTK_CSS
                    fi
                done
                if [[ ! -s $GTK_CSS ]]; then
                    echo "" > $GTK_CSS
                    sed -i "s|^$|$RESIZING_IMPORT|" $GTK_CSS
                else
                    sed -i "1i $RESIZING_IMPORT" $GTK_CSS
                fi
            fi
        fi
    fi

#-----------------------------------------------------------------------------------------------

# Custom-Color
elif [[ $COLOR == ${colors[1]} ]]; then

    # Squeekboard-Colors
    cp $SQUEEKBOARD_COLORS $OSK

    # Styles
    if [[ ! -f $STYLE_CSS ]]; then
        for STYLE_FILE in "${style_locations[@]}"; do
            if [[ -f $STYLE_FILE ]]; then
                if grep -q "$SB_COLORS_IMPORT" "$STYLE_FILE"; then
                    sed -i "\|$SB_COLORS_IMPORT|d" $STYLE_FILE
                    PHOSH=${phosh[2]}
                fi
                for RS_IMPORT in "${style_resizing_imports[@]}"; do
                    if grep -q "$RS_IMPORT" "$STYLE_FILE"; then
                        sed -i "\|$RS_IMPORT|d" $STYLE_FILE
                        PHOSH=${phosh[2]}
                    fi
                done
                COUNT=$(grep -o "@import" "$STYLE_FILE" | wc -l)
                if [ "$COUNT" -le 1 ] ; then
                    rm $STYLE_FILE
                fi
            fi
        done
        cp $REPO_STYLE_CSS $THEMES
        PHOSH=${phosh[2]}
    fi
    if ! grep -q "$THEME_IMPORT" "$STYLE_CSS"; then
    for THEME_FILE_IMPORT in "${theme_imports[@]}"; do
        sed -i "\|$THEME_FILE_IMPORT|d" $STYLE_CSS
    done
        sed -i "4i $THEME_IMPORT" $STYLE_CSS
        PHOSH=${phosh[2]}
    fi
    if ! grep -q "$SB_COLORS_IMPORT" "$STYLE_CSS"; then
        sed -i "5i $SB_COLORS_IMPORT" $STYLE_CSS
        PHOSH=${phosh[2]}
    fi
    if ! grep -q "$SBRS" "$STYLE_CSS"; then
        sed -i "6i $STYLE_RESIZING_IMPORT" $STYLE_CSS
        PHOSH=${phosh[2]}
    fi

    # Themes
    if [[ ! -d $COLORS ]]; then
        mkdir -p $COLORS
        PHOSH=${phosh[2]}
    fi
    if [[ ! -f $THEME_CSS ]]; then
        for ((i = 0; i < ${#themes[@]}; ++i)); do
            THEME_FILE=$(echo "${theme_locations[$i]}")
            THEME_FILE_IMPORT=$(echo "${theme_imports[$i]}")
            if [[ -f $THEME_FILE ]]; then
                TAG="REMOVE"
                for STYLE_FILE in "${style_locations[@]}"; do
                    if [[ -f $STYLE_FILE ]]; then
                        if grep -q "$THEME_FILE_IMPORT" "$STYLE_FILE"; then
                            TAG="IGNORE"
                        fi
                    fi
                done
                if [[ $TAG == "REMOVE" ]]; then
                    rm $THEME_FILE
                fi
            fi
        done
    fi
    cp $REPO_THEME_CSS $COLORS

    # Scales
    if [[ $SCALE == ${scales[1]} ]]; then
        sed -i "s|$SCALE_0|$SCALE_1|" $STYLE_CSS
        PHOSH=${phosh[2]}
    elif [[ $SCALE == ${scales[2]} ]]; then
        sed -i "s|$SCALE_0|$SCALE_2|" $STYLE_CSS
        PHOSH=${phosh[2]}
    elif [[ $SCALE == ${scales[3]} ]]; then
        sed -i "s|$SCALE_0|$SCALE_3|" $STYLE_CSS
        PHOSH=${phosh[2]}
    fi

    # If gtk.css does not yet exist
    if [[ ! -f $GTK_CSS ]]; then
        echo "" > $GTK_CSS
        PHOSH=${phosh[2]}

        # Add selected style to gtk.css
        sed -i "s|^$|$STYLE_IMPORT|" $GTK_CSS

    # If gtk.css exists
    elif [[ -f $GTK_CSS ]]; then

        # Remove Squeekboard Resizing from gtk.css if it is present
        for RS_IMPORT in "${resizing_imports[@]}"; do
            if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                sed -i "\|$RS_IMPORT|d" $GTK_CSS
            fi
        done

        # Remove any previously selected styles from gtk.css
        if ! grep -q "$STYLE_IMPORT" "$GTK_CSS"; then
            for STYLE_FILE_IMPORT in "${style_imports[@]}"; do
                if grep -q "$STYLE_FILE_IMPORT" "$GTK_CSS"; then
                    sed -i "\|$STYLE_FILE_IMPORT|d" $GTK_CSS
                fi
            done

            # Add selected style to gtk.css
            if [[ ! -s $GTK_CSS ]]; then
                echo "" > $GTK_CSS
                sed -i "s|^$|$STYLE_IMPORT|" $GTK_CSS
            else
                sed -i "1i $STYLE_IMPORT" $GTK_CSS
            fi
            PHOSH=${phosh[2]}
        fi
    fi
fi

################################################################################################

# Inform user of install completion and prompt to continue

if [[ -f $EVDEV_XML_BACKUP && -d $KEYBOARDS_BACKUP && -f $GTK_CSS_BACKUP ]]; then
    clear
    echo ""
    echo " Squeekboard-Layouts install completed"
    echo ""
    echo " You can use this install script again"
    echo " & choose a new style whenever you want"
    echo ""
    echo "  Remember that your backups are here:"
    echo ""
    echo "   $KEYBOARDS_BACKUP"
    echo " $GTK_CSS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo " The uninstall script will restore them"
    echo ""
    echo "     * Press any key to continue *"
    echo ""

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP && -d $KEYBOARDS_BACKUP ]]; then
    clear
    echo ""
    echo " Squeekboard-Layouts install completed"
    echo ""
    echo " You can use this install script again"
    echo " & choose a new style whenever you want"
    echo ""
    echo "  Remember that your backups are here:"
    echo ""
    echo "   $KEYBOARDS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo " The uninstall script will restore them"
    echo ""
    echo "     * Press any key to continue *"
    echo ""
    echo ""

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP && -f $GTK_CSS_BACKUP ]]; then
    clear
    echo ""
    echo " Squeekboard-Layouts install completed"
    echo ""
    echo " You can use this install script again"
    echo " & choose a new style whenever you want"
    echo ""
    echo "  Remember that your backups are here:"
    echo ""
    echo " $GTK_CSS_BACKUP"
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo " The uninstall script will restore them"
    echo ""
    echo "     * Press any key to continue *"
    echo ""
    echo ""

#-----------------------------------------------------------------------------------------------

elif [[ -f $EVDEV_XML_BACKUP ]]; then
    clear
    echo ""
    echo " Squeekboard-Layouts install completed"
    echo ""
    echo " You can use this install script again"
    echo " & choose a new style whenever you want"
    echo ""
    echo "   Remember that your backup is here:"
    echo ""
    echo "$EVDEV_XML_BACKUP"
    echo ""
    echo "  The uninstall script will restore it"
    echo ""
    echo "     * Press any key to continue *"
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        *)
            ;;
    esac
fi

################################################################################################

# Inform user of steps to enable Squeekboard-Layouts keyboard

# If phosh restart is not required or recommended
if [[ $PHOSH == ${phosh[0]} ]]; then
    clear
    echo ""
    echo "To enable Squeekboard-Layouts keyboard:"
    echo ""
    echo "  1. Launch the Settings application"
    echo "  2. Select \"Region & Language\" menu"
    echo "  3. Click \"+\" under \"Input Sources\""
    echo "  4. Click the language you selected"
    echo "     [Default is \"German (Germany)\"]"
    echo "  5. Click \"$LANGUAGE (Librem 5)\""
    echo "  6. Click \"Add\" button at top right"
    echo "  7. Click globe button on keyboard"
    echo "  8. Click \"$LANGUAGE (Librem 5)\""
    echo ""
    echo ""
    echo ""
    echo ""

#-----------------------------------------------------------------------------------------------

# If phosh restart is recommended or required
elif [[ $PHOSH == ${phosh[1]} || $PHOSH == ${phosh[2]} ]]; then
    clear
    echo ""
    echo "To enable Squeekboard-Layouts keyboard:"
    echo ""
    echo "  1. Launch the Settings application"
    echo "  2. Select \"Region & Language\" menu"
    echo "  3. Click \"+\" under \"Input Sources\""
    echo "  4. Click the language you selected"
    echo "     [Default is \"German (Germany)\"]"
    echo "  5. Click \"$LANGUAGE (Librem 5)\""
    echo "  6. Click \"Add\" button at top right"
    echo "  7. Click globe button on keyboard"
    echo "  8. Click \"$LANGUAGE (Librem 5)\""
    echo ""
    echo "     * Press any key to continue *"
    echo ""
    echo ""
    read a;
    case $a in
        *)
            ;;
    esac
fi

################################################################################################

# Inform user of phosh restart necessity

# Phosh restart recommended or required
if [[ $PHOSH == ${phosh[1]} || $PHOSH == ${phosh[2]} ]]; then
    clear
    echo ""
    echo "$PHOSH"
    echo ""
    echo ""
    echo ""
    echo " y) * Restart phosh now*"
    echo " n)   I'll do it later"
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            RESTART=${restart[0]}
            ;;
        n|N)
            clear
            echo ""
            echo "   When you decide to restart phosh,"
            echo "    run this command in a terminal:"
            echo ""
            echo "        systemctl restart phosh"
            echo ""
            echo ""
            echo ""
            echo "      * Press any key to finish *"
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            echo ""
            read a;
            case $a in
                *)
                    clear
                    exit
                    ;;
            esac
            ;;
    esac
fi

################################################################################################

# Restart phosh
if [[ $RESTART == ${restart[0]} ]]; then
    systemctl restart phosh
fi

################################################################################################
