# Squeekboard-Layouts

Here you can find Squeekboard custom layouts for all 5 common screen scales. The main layout is German. New: I also support US-layouts right now. However, if you want to modify it for your own language (but same style) and if you want to share it for others, create a merge request or share it on forums.puri.sm and ping @ick. Recolorization is also welcome.

![Pink Glass color theme](Custom-Color/Screenshots/ScreenshotsOSK.png "Pink Glass color theme - Portrait View")
![Pink Glass color theme Landscape](Custom-Color/Screenshots/ScreenshotsOSK_wide.png "Pink Glass color theme - Landscape View")


## Automated Install

#### Option 1:

1.  Clone this repo:

```
git clone https://codeberg.org/Ick/Squeekboard-Layouts.git
```

2. Make the install script executable:

```
chmod +x Squeekboard-Layouts/install.sh
```

3. Run the install script:

```
bash Squeekboard-Layouts/install.sh
```

#### Option 2:

1. Download an archive of this repo from one of these links:

* [TAR Archive](https://codeberg.org/ick/Squeekboard-Layouts/archive/main.tar.gz)
* [ZIP Archive](https://codeberg.org/ick/Squeekboard-Layouts/archive/main.zip)

2. Extract the archive using a file manager or use this command (for TAR archives):

```
tar -xf Squeekboard-Layouts-master.tar.gz
```

3. Make the install script executable:

```
chmod +x Squeekboard-Layouts/install.sh
```

4. Run the install script using a file manager or use this command:

```
bash Squeekboard-Layouts/install.sh
```


## Automated Uninstall

1. Make the uninstall script executable:

```
chmod +x Squeekboard-Layouts/uninstall.sh
```

2. Run the uninstall script:

```
bash Squeekboard-Layouts/uninstall.sh
```


# Manual Install

Set up layout

1. Open https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main/evdev-lines/
2. Open the file of your language (for German `de+l5.xml`).
3. Follow the description in its comment.

After this is done, you can choose the new layout from the settings app as every other preinstalled layout.

Installing the layout

1. Choose if you want your layout in default colors or if you want the custom color version. Open one of those directories here: https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main
2. Choose the directory that matches your screen scale best.
3. Download the file in your language (`de+l5.yml` for German) and save it into `~/.local/share/squeekboard/keyboards/`. Create this directory if not already done.
4. If you want to use this layout also on terminal, create another folder with the name terminal and save the downloaded file there, too.

After this step you already can use your layout. However, there are further improvements.

(Optional) Adjust size to your specific screen scale. It affects font size and button borders. Install colors if you have choosen its layout.

1. Open https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main/gtk-3.0/themes/osk and the folder of your screen scale.
2. Download this file and save it into `~/config/gtk-3.0/themes/osk/`

If you have choosen the default color variant:

3. Open https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main/gtk-3.0
4. Open directory `~/config/gtk-3.0/` and create `gtk.css`, if not already done. Insert `@import url("themes/osk/scale_1.0_and_2.0/squeekboard-colors.css");` to a new line.
5. Edit the url of step 4 to match your choosen scale.

If you have choosen the custom color variant:

3. Open https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main/gtk-3.0/themes/colors
4. Download a file you want and save it into `~/config/gtk-3.0/themes/colors/`
5. Open https://codeberg.org/Ick/Squeekboard-Layouts/src/branch/main/gtk-3.0/themes
6. Download `glassy-style.css`
7. Edit this file and replace in line `@import url("osk/scale_1.0_and_2.0/squeekboard-colors.css");` the scale with your actual choosen scale.
8. Open directory `~/config/gtk-3.0/` and create `gtk.css`, if not already done. Insert `@import url("themes/glassy-style.css");` to a new line.

Now everything important is set up. 


# Options

You also can download the whole `gtk-3.0` folder and at everything to your system. In this case open `~/config/gtk-3.0/themes/glassy-style.css`. Here you can choose what ever file for scale and color should be choosen. 

You also can edit the color file itself in `~/config/gtk-3.0/themes/colors/`. Edit the section `Main Theme Colors` to change the custom theme wide default colors. Edit the section `Squeekboard` to change details.


# Manual Uninstall

Follow installation steps and just remove where and what you had to add. 
