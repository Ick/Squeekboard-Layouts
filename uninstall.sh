#!/bin/bash

#############################################
# Copyright 2023 user0                      #
# SPDX-License-Identifier: GPL-3.0-or-later #
################################################################################################

# Directories & Files

# XML File
EVDEV_XML=/usr/share/X11/xkb/rules/evdev.xml

# Squeekboard directories
KEYBOARDS=~/.local/share/squeekboard/keyboards

# GTK directories
GTK_3=~/.config/gtk-3.0
THEMES=$GTK_3/themes
COLORS=$THEMES/colors
OSK=$THEMES/osk
SCALE_100=$OSK/scale_1.0_and_2.0
SCALE_125=$OSK/scale_1.25
SCALE_150=$OSK/scale_1.5
SCALE_175=$OSK/scale_1.75

# GTK files
GTK_CSS=$GTK_3/gtk.css
STYLE_00=$THEMES/glassy-style.css
THEME_00=$COLORS/pink-glass.css
THEME_01=$COLORS/red-glass.css
THEME_02=$COLORS/orange-glass.css
THEME_03=$COLORS/yellow-glass.css
THEME_04=$COLORS/green-glass.css
THEME_05=$COLORS/blue-glass.css
THEME_06=$COLORS/purple-glass.css
THEME_07=$COLORS/berry-glass.css
THEME_08=$COLORS/mint_light-glass.css
THEME_09=$COLORS/brown-glass.css
SB_COLORS=$OSK/squeekboard_colors.css
SB_RS_100=$SCALE_100/squeekboard_resizing.css
SB_RS_125=$SCALE_125/squeekboard_resizing.css
SB_RS_150=$SCALE_150/squeekboard_resizing.css
SB_RS_175=$SCALE_175/squeekboard_resizing.css

# Backups
EVDEV_XML_BACKUP=/usr/share/X11/xkb/rules/evdev.xml.bak
KEYBOARDS_BACKUP=~/.keyboards.backup/
GTK_CSS_BACKUP=$GTK_3/gtk.bak

################################################################################################

# Arrays & Variables

# resizing_imports array
resizing_imports[0]="@import url(\"themes/osk/scale_1.0_and_2.0/squeekboard_resizing.css\");"
resizing_imports[1]="@import url(\"themes/osk/scale_1.25/squeekboard_resizing.css\");"
resizing_imports[2]="@import url(\"themes/osk/scale_1.5/squeekboard_resizing.css\");"
resizing_imports[3]="@import url(\"themes/osk/scale_1.75/squeekboard_resizing.css\");"

# style_resizing_imports array
style_resizing_imports[0]="@import url(\"osk/scale_1.0_and_2.0/squeekboard_resizing.css\");"
style_resizing_imports[1]="@import url(\"osk/scale_1.25/squeekboard_resizing.css\");"
style_resizing_imports[2]="@import url(\"osk/scale_1.5/squeekboard_resizing.css\");"
style_resizing_imports[3]="@import url(\"osk/scale_1.75/squeekboard_resizing.css\");"

# resizing_locations array
resizing_locations[0]="$SB_RS_100"
resizing_locations[1]="$SB_RS_125"
resizing_locations[2]="$SB_RS_150"
resizing_locations[3]="$SB_RS_175"

# theme_imports array
theme_imports[0]="@import url(\"colors/pink-glass.css\");"
theme_imports[1]="@import url(\"colors/red-glass.css\");"
theme_imports[2]="@import url(\"colors/orange-glass.css\");"
theme_imports[3]="@import url(\"colors/yellow-glass.css\");"
theme_imports[4]="@import url(\"colors/green-glass.css\");"
theme_imports[5]="@import url(\"colors/blue-glass.css\");"
theme_imports[6]="@import url(\"colors/purple-glass.css\");"
theme_imports[7]="@import url(\"colors/berry-glass.css\");"
theme_imports[8]="@import url(\"colors/mint_light-glass.css\");"
theme_imports[9]="@import url(\"colors/brown-glass.css\");"

# theme_locations array
theme_locations[0]="$THEME_00"
theme_locations[1]="$THEME_01"
theme_locations[2]="$THEME_02"
theme_locations[3]="$THEME_03"
theme_locations[4]="$THEME_04"
theme_locations[5]="$THEME_05"
theme_locations[6]="$THEME_06"
theme_locations[7]="$THEME_07"
theme_locations[8]="$THEME_08"
theme_locations[9]="$THEME_09"

# style_imports array
style_imports[0]="@import url(\"themes/glassy-style.css\");"

# style_locations array
style_locations[0]="$STYLE_00"

# backups array
backups[0]="No backups"
backups[1]="Yes backups"

# phosh array
phosh[0]="No restart required"
phosh[1]="Restart phosh to apply latest CSS files"
phosh[2]="Phosh must be restarted to apply layout"

# restart array
restart[0]="Restart phosh now"

# variables
declare -- BACKUPS
declare -- PHOSH
declare -- RESTART

SB_COLORS_IMPORT="@import url(\"osk/squeekboard_colors.css\");"

################################################################################################

# Inform user that Squeekboard-Layouts is not installed

if ! grep -q "(Librem 5)" "$EVDEV_XML"; then
    clear
    echo ""
    echo "   Squeekboard-Layouts not installed"
    echo ""
    echo "      * Press any key to exit *"
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        *)
            clear
            exit
            ;;
    esac
fi

################################################################################################

# Ask user for confirmation

# No backups could be located
if [[ ! -f $EVDEV_XML_BACKUP && ! -d $KEYBOARDS_BACKUP && ! -f $GTK_CSS_BACKUP ]]; then
    BACKUPS=${backups[0]}
    clear
    echo ""
    echo "     Uninstall Squeekboard-Layouts?"
    echo ""
    echo ""
    echo ""
    echo ""
    echo " y)   Yes"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        q|Q)
            clear
            exit
            ;;
        y|Y|'')
            clear
            echo ""
            echo "     A backup could not be located"
            echo ""
            echo "            Continue anyway?"
            echo ""
            echo ""
            echo " y)   Yes"
            echo " q)   I change my mind; quit."
            echo ""
            echo ""
            echo ""
            echo ""
            read a;
            case $a in
                q|Q)
                    clear
                    exit
                    ;;
                y|Y|'')
                    PHOSH=${phosh[0]}
                    rm -rf $KEYBOARDS
                    if [[ -f $SB_COLORS ]]; then
                        rm $SB_COLORS
                    fi
                    for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                        STYLE_CSS=$(echo "${style_locations[$i]}")
                        STYLE_IMPORTS=$(echo "${style_imports[$i]}")
                        if [[ -f $STYLE_CSS ]]; then
                            if grep -q "$SB_COLORS_IMPORT" "$STYLE_CSS"; then
                                sed -i "\|$SB_COLORS_IMPORT|d" $STYLE_CSS
                                PHOSH=${phosh[2]}
                            fi
                            for ((i = 0; i < ${#style_resizing_imports[@]}; ++i)); do
                                RS_IMPORT=$(echo "${style_resizing_imports[$i]}")
                                if grep -q "$RS_IMPORT" "$STYLE_CSS"; then
                                    sed -i "\|$RS_IMPORT|d" $STYLE_CSS
                                    PHOSH=${phosh[2]}
                                fi
                            done
                            COUNT=$(grep -o "@import" "$STYLE_CSS" | wc -l)
                            if [ $COUNT -le 1 ] ; then
                                rm $STYLE_CSS
                                for ((i = 0; i < ${#style_imports[@]}; ++i)); do
                                    STYLE_IMPORT=$(echo "${style_imports[$i]}")
                                    if grep -q "$STYLE_IMPORT" "$GTK_CSS"; then
                                        sed -i "\|$STYLE_IMPORT|d" $GTK_CSS
                                        PHOSH=${phosh[2]}
                                    fi
                                done
                            fi
                        fi
                    done
                    for ((i = 0; i < ${#theme_locations[@]}; ++i)); do
                        THEME_CSS=$(echo "${theme_locations[$i]}")
                        THEME_IMPORT=$(echo "${theme_imports[$i]}")
                        if [[ -f $THEME_CSS ]]; then
                            TAG="REMOVE"
                            for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                                STYLE_CSS=$(echo "${style_locations[$i]}")
                                if [[ -f $STYLE_CSS ]]; then
                                    if grep -q "$THEME_IMPORT" "$STYLE_CSS"; then
                                        TAG="IGNORE"
                                    fi
                                fi
                            done
                            if [[ $TAG == "REMOVE" ]]; then
                                rm $THEME_CSS
                            fi
                        fi
                    done
                    if [[ -d $COLORS ]]; then
                        if [[ ! $(ls -A $COLORS) ]]; then
                            rm -rf $COLORS
                        fi
                    fi
                    if [[ -d $OSK ]]; then
                        rm -rf $OSK
                    fi
                    for ((i = 0; i < ${#resizing_imports[@]}; ++i)); do
                        RS_IMPORT=$(echo "${resizing_imports[$i]}")
                        if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                            sed -i "\|$RS_IMPORT|d" $GTK_CSS
                            PHOSH=${phosh[2]}
                        fi
                    done
                    if ! grep -q "@import" "$GTK_CSS"; then
                        rm $GTK_CSS
                    fi
                    if [[ -d $THEMES ]]; then
                        if [[ ! $(ls -A $THEMES) ]]; then
                            rm -rf $THEMES
                        fi
                    fi
                    ;;
            esac
            ;;
    esac

#-----------------------------------------------------------------------------------------------

# Main backup could not be located
elif [[ ! -f $EVDEV_XML_BACKUP ]]; then
    BACKUPS=${backups[1]}
    clear
    echo ""
    echo "     Uninstall Squeekboard-Layouts?"
    echo ""
    echo ""
    echo ""
    echo ""
    echo " y)   Yes"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        q|Q)
            clear
            exit
            ;;
        y|Y|'')
            clear
            echo ""
            echo "  Your backup is ready to be restored"
            echo ""
            echo "               Continue?"
            echo ""
            echo ""
            echo " y)   Yes"
            echo " q)   I change my mind; quit."
            echo ""
            echo ""
            echo ""
            echo ""
            read a;
            case $a in
                q|Q)
                    clear
                    exit
                    ;;
                y|Y|'')
                    PHOSH=${phosh[0]}
                    rm -rf $KEYBOARDS
                    if [[ -d $KEYBOARDS_BACKUP ]]; then
                        cp -rf $KEYBOARDS_BACKUP $KEYBOARDS
                    fi
                    if [[ -f $GTK_CSS_BACKUP ]]; then
                        cp $GTK_CSS_BACKUP $GTK_CSS
                        PHOSH=${phosh[2]}
                    fi
                    if [[ -f $SB_COLORS ]]; then
                        rm $SB_COLORS
                    fi
                    for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                        STYLE_CSS=$(echo "${style_locations[$i]}")
                        STYLE_IMPORTS=$(echo "${style_imports[$i]}")
                        if [[ -f $STYLE_CSS ]]; then
                            if grep -q "$SB_COLORS_IMPORT" "$STYLE_CSS"; then
                                sed -i "\|$SB_COLORS_IMPORT|d" $STYLE_CSS
                                PHOSH=${phosh[2]}
                            fi
                            for ((i = 0; i < ${#style_resizing_imports[@]}; ++i)); do
                                RS_IMPORT=$(echo "${style_resizing_imports[$i]}")
                                if grep -q "$RS_IMPORT" "$STYLE_CSS"; then
                                    sed -i "\|$RS_IMPORT|d" $STYLE_CSS
                                    PHOSH=${phosh[2]}
                                fi
                            done
                            COUNT=$(grep -o "@import" "$STYLE_CSS" | wc -l)
                            if [ $COUNT -le 1 ] ; then
                                rm $STYLE_CSS
                                for ((i = 0; i < ${#style_imports[@]}; ++i)); do
                                    STYLE_IMPORT=$(echo "${style_imports[$i]}")
                                    if grep -q "$STYLE_IMPORT" "$GTK_CSS"; then
                                        sed -i "\|$STYLE_IMPORT|d" $GTK_CSS
                                        PHOSH=${phosh[2]}
                                    fi
                                done
                            fi
                        fi
                    done
                    for ((i = 0; i < ${#theme_locations[@]}; ++i)); do
                        THEME_CSS=$(echo "${theme_locations[$i]}")
                        THEME_IMPORT=$(echo "${theme_imports[$i]}")
                        if [[ -f $THEME_CSS ]]; then
                            TAG="REMOVE"
                            for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                                STYLE_CSS=$(echo "${style_locations[$i]}")
                                if [[ -f $STYLE_CSS ]]; then
                                    if grep -q "$THEME_IMPORT" "$STYLE_CSS"; then
                                        TAG="IGNORE"
                                    fi
                                fi
                            done
                            if [[ $TAG == "REMOVE" ]]; then
                                rm $THEME_CSS
                            fi
                        fi
                    done
                    if [[ -d $COLORS ]]; then
                        if [[ ! $(ls -A $COLORS) ]]; then
                            rm -rf $COLORS
                        fi
                    fi
                    if [[ -d $OSK ]]; then
                        rm -rf $OSK
                    fi
                    for ((i = 0; i < ${#resizing_imports[@]}; ++i)); do
                        RS_IMPORT=$(echo "${resizing_imports[$i]}")
                        if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                            sed -i "\|$RS_IMPORT|d" $GTK_CSS
                            PHOSH=${phosh[2]}
                        fi
                    done
                    if ! grep -q "@import" "$GTK_CSS"; then
                        rm $GTK_CSS
                    fi
                    if [[ -d $THEMES ]]; then
                        if [[ ! $(ls -A $THEMES) ]]; then
                            rm -rf $THEMES
                        fi
                    fi
                    ;;
            esac
            ;;
    esac

#-----------------------------------------------------------------------------------------------

# Main backup located
elif [[ -f $EVDEV_XML_BACKUP ]]; then
    BACKUPS=${backups[1]}
    clear
    echo ""
    echo "    Uninstall Squeekboard-Layouts?"
    echo ""
    echo ""
    echo ""
    echo ""
    echo " y)   Yes"
    echo " q)   I change my mind; quit."
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        q|Q)
            clear
            exit
            ;;
        y|Y|'')
            clear
            echo ""
            echo "  Your backup is ready to be restored"
            echo ""
            echo "               Continue?"
            echo ""
            echo ""
            echo " y)   Yes"
            echo " q)   I change my mind; quit."
            echo ""
            echo ""
            echo ""
            echo ""
            read a;
            case $a in
                q|Q)
                    clear
                    exit
                    ;;
                y|Y|'')
                    clear
                    echo ""
                    echo " You will be prompted for a password"
                    echo " in order to edit the following file:"
                    echo ""
                    echo "  $EVDEV_XML"
                    echo ""
                    echo "    * Press any key to continue *"
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    read a;
                    case $a in
                        *)
                            PHOSH=${phosh[0]}
                            sudo cp $EVDEV_XML_BACKUP $EVDEV_XML
                            rm -rf $KEYBOARDS
                            if [[ -d $KEYBOARDS_BACKUP ]]; then
                                cp -rf $KEYBOARDS_BACKUP $KEYBOARDS
                            fi
                            if [[ -f $GTK_CSS_BACKUP ]]; then
                                cp $GTK_CSS_BACKUP $GTK_CSS
                                PHOSH=${phosh[2]}
                            fi
                            if [[ -f $SB_COLORS ]]; then
                                rm $SB_COLORS
                            fi
                            for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                                STYLE_CSS=$(echo "${style_locations[$i]}")
                                STYLE_IMPORTS=$(echo "${style_imports[$i]}")
                                if [[ -f $STYLE_CSS ]]; then
                                    if grep -q "$SB_COLORS_IMPORT" "$STYLE_CSS"; then
                                        sed -i "\|$SB_COLORS_IMPORT|d" $STYLE_CSS
                                        PHOSH=${phosh[2]}
                                    fi
                                    for ((i = 0; i < ${#style_resizing_imports[@]}; ++i)); do
                                        RS_IMPORT=$(echo "${style_resizing_imports[$i]}")
                                        if grep -q "$RS_IMPORT" "$STYLE_CSS"; then
                                            sed -i "\|$RS_IMPORT|d" $STYLE_CSS
                                            PHOSH=${phosh[2]}
                                        fi
                                    done
                                    COUNT=$(grep -o "@import" "$STYLE_CSS" | wc -l)
                                    if [ $COUNT -le 1 ] ; then
                                        rm $STYLE_CSS
                                        for ((i = 0; i < ${#style_imports[@]}; ++i)); do
                                            STYLE_IMPORT=$(echo "${style_imports[$i]}")
                                            if grep -q "$STYLE_IMPORT" "$GTK_CSS"; then
                                                sed -i "\|$STYLE_IMPORT|d" $GTK_CSS
                                                PHOSH=${phosh[2]}
                                            fi
                                        done
                                    fi
                                fi
                            done
                            for ((i = 0; i < ${#theme_locations[@]}; ++i)); do
                                THEME_CSS=$(echo "${theme_locations[$i]}")
                                THEME_IMPORT=$(echo "${theme_imports[$i]}")
                                if [[ -f $THEME_CSS ]]; then
                                    TAG="REMOVE"
                                    for ((i = 0; i < ${#style_locations[@]}; ++i)); do
                                        STYLE_CSS=$(echo "${style_locations[$i]}")
                                        if [[ -f $STYLE_CSS ]]; then
                                            if grep -q "$THEME_IMPORT" "$STYLE_CSS"; then
                                                TAG="IGNORE"
                                            fi
                                        fi
                                    done
                                    if [[ $TAG == "REMOVE" ]]; then
                                        rm $THEME_CSS
                                    fi
                                fi
                            done
                            if [[ -d $COLORS ]]; then
                                if [[ ! $(ls -A $COLORS) ]]; then
                                    rm -rf $COLORS
                                fi
                            fi
                            if [[ -d $OSK ]]; then
                                rm -rf $OSK
                            fi
                            for ((i = 0; i < ${#resizing_imports[@]}; ++i)); do
                                RS_IMPORT=$(echo "${resizing_imports[$i]}")
                                if grep -q "$RS_IMPORT" "$GTK_CSS"; then
                                    sed -i "\|$RS_IMPORT|d" $GTK_CSS
                                    PHOSH=${phosh[2]}
                                fi
                            done
                            if ! grep -q "@import" "$GTK_CSS"; then
                                rm $GTK_CSS
                            fi
                            if [[ -d $THEMES ]]; then
                                if [[ ! $(ls -A $THEMES) ]]; then
                                    rm -rf $THEMES
                                fi
                            fi
                            ;;
                    esac
                    ;;
            esac
            ;;
    esac

fi

################################################################################################

# Inform user of uninstall completion

# Phosh restart not required
if [[ $PHOSH == ${phosh[0]} ]]; then
    if [[ $BACKUPS == ${backups[0]} ]]; then
        clear
        echo ""
        echo "   Squeekboard-Layouts uninstalled"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
    elif [[ $BACKUPS == ${backups[1]} ]]; then
        clear
        echo ""
        echo "   Squeekboard-Layouts uninstalled"
        echo ""
        echo " Your pre-install backup is restored"
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
        echo ""
    fi
fi

################################################################################################

# Inform user of phosh restart necessity

# Phosh restart recommended or required
if [[ $PHOSH == ${phosh[1]} || $PHOSH == ${phosh[2]} ]]; then
    clear
    echo ""
    echo "$PHOSH"
    echo ""
    echo ""
    echo ""
    echo ""
    echo " y) * Restart phosh now *"
    echo " n)   I'll do it later"
    echo ""
    echo ""
    echo ""
    echo ""
    read a;
    case $a in
        y|Y|'')
            RESTART=${restart[0]}
            ;;
        n|N)
            clear
            echo ""
            echo "   When you decide to restart phosh,"
            echo "    run this command in a terminal:"
            echo ""
            echo "        systemctl restart phosh"
            echo ""
            echo ""
            echo ""
            echo "      * Press any key to finish *"
            echo ""
            echo ""
            echo ""
            read a;
            case $a in
                *)
                    clear
                    echo ""
                    echo "   Squeekboard-Layouts uninstalled"
                    echo ""
                    echo " Your pre-install backup is restored"
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    echo ""
                    ;;
            esac
            ;;
    esac
fi

################################################################################################

# Restart phosh
if [[ $RESTART == ${restart[0]} ]]; then
    systemctl restart phosh
fi

################################################################################################
